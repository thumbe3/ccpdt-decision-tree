#include<iostream>
#include<string>
#include<bits/stdc++.h>
#include<stdio.h>
using namespace std;

class DTNode
{
	 private:
	 	int feature;
	 	string attr;
	 	int type;
	 	double thres;
	 	bool isleaf;
	 	string clss;
	 	vector<DTNode*> children;

	 public:
	 	DTNode();
	 	int getFeature();
	 	int getType();
	 	bool isLeaf();
	 	string getClass();
	 	double getThres();
	 	vector<DTNode*> getChildren();


	 	void setType(int type);
	 	void setFeature(int feature);
	 	void setLeaf(bool isleaf);
	 	void setClass(string clss);
	 	void setThres(double thres);
	 	void setChildren(vector<DTNode*> children);
};







class DecisionTree
{
	private:
		DTNode* root;
		vector<vector<string> > data;
		vector<int> types;
		vector<vector<string> > metaD;
		vector<vector<double> > metaC;
		map<string,int> classes;
	public:
		DecisionTree(vector<vector<string> > data,vector<vector<string> > metaD,vector<vector<double> > metaC, vector<int> types);
		double contInfoGain(int f,double thres,vector<int> examples);
		void Build(vector<int> examples,DTNode* root,vector<int> featUsed,string majorityclss);	
		void Test(vector<vector<string> > testData, DTNode* root);

};	
//DTNode* root, vector<string,vector<string> > data


class loadData
{	
public:
	static vector<vector<string> > load(string fileName);
	static vector<vector<double> > loadMeta(vector<vector<string> > data);


};