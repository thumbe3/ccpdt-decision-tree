#include "DecisionTree.h"
#include <bits/stdc++.h>
#define pb push_back
#define forn(i,n) for(int i=0; i<int(n);i++)


int main()
{
	vector<vector<string>> data=loadData::load("page-blocks.data");
	vector<vector<double> > metaC=loadData::loadMeta(data);
	vector<int> featUsed;	// if not used then 0 otherwise 1
	vector<int> examples;
	vector<int> types;		// type 0 is continous and 1 is discrete
	int numFeat=data.size();
	int numExamples=data[0].size();




	vector<vector<string> > metaD;
	vector<string> clss={"1","2","3","4","5"};
	vector<int> totalCount={2700,160,15,50,70};
	vector<int> curr={0,0,0,0,0};
	metaD.pb(clss);

	vector<vector<string> > train(data.size());
	vector<vector<string> > test(data.size());

	forn(i,data[0].size())
	{
		int clss=data.back()[i][0]-'1';
		
		if(curr[clss]<totalCount[clss])
		{
			forn(j,data.size())
				train[j].pb(data[j][i]);
			curr[clss]++;
		}
		else
		{
			forn(j,data.size())
				test[j].pb(data[j][i]);
		}
	}
   	forn(i,train[0].size())
		featUsed.pb(0),types.pb(0);

	forn(i,train[0].size())
		examples.pb(i);

	DTNode* root= new DTNode();

	DecisionTree* dt=new DecisionTree(data,metaD,metaC,types);
	dt->Build(examples,root,featUsed,"1");
	dt->Test(test,root);



	return 0;
}