#include "DecisionTree.h"
#include<bits/stdc++.h>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#define forn(i,n) for(int i=0;i<int(n);i++)
#define forn1(i,n) for(int i=1;i<int(n);i++)
#define pb push_back
//////////// DT Node definitions //////////////////////////////
DTNode::DTNode()
{
	;
}
int DTNode::getFeature()
{
	return this->feature;
} 
void DTNode::setFeature(int feature)
{
	this->feature=feature;
}

double DTNode::getThres()
{
	return this->thres;
}
void DTNode::setThres(double thres)
{
	this->thres=thres;
}

int DTNode::getType()
{
	return this->type;
}
void DTNode::setType(int type)
{
	this->type=type;
}


string DTNode::getClass()
{
	return this->clss;
}
void DTNode::setClass(string clss)
{
	this->clss=clss;
}


bool DTNode::isLeaf()
{
	return this->isleaf;
}
void DTNode::setLeaf(bool isleaf)
{
	this->isleaf=isleaf;
}

vector<DTNode*> DTNode::getChildren()
{
	return this->children;
}
void DTNode::setChildren(vector<DTNode*> children)
{
	this->children=children;
}




////////////////Decision tree function declarations ////////////
DecisionTree::DecisionTree(vector<vector<string> > data,vector<vector<string> > metaD, vector<vector<double> > metaC, vector<int> types)
		{
			this->root=NULL;
			this->data=data;
			this->types=types;
			this->metaD=metaD;
			this->metaC=metaC;
			vector<string> temp=metaD.back();
			forn(i,temp.size())
				classes[temp[i]]=i;
		}

double xlogx(double x)
{
	if(x==0)
		return 0;
    return -x*log10(x);
}




//CCPDT Info gain
// Vector of the form [[p1,n1],[p2,n2],...]]
double C5CalcEntropy(vector<vector<int> > a)
{
	vector<double> suma;
	double total=0;

	forn(i,a.size())
		{
			double sum=0;
			forn(j,a[i].size())
				sum+=1.0*a[i][j];
			suma.pb(sum);
			total+=sum;		
		}
	double ent=0,totalEnt=0;

	forn(i,a.size())
	{
		ent=0;
		forn(j,a[i].size())
		{
			ent+=xlogx(1.0*(a[i][j]/suma[i]));
		}
		totalEnt+=ent*suma[i]/total;
	}
	return totalEnt;

}

double calcEntropy(vector<vector<int> > a)
{
        vector<double> suma;
        double ans=0;

        forn(i,a[0].size())
                suma.pb(1.0*a[0][i]);

        forn1(i,a.size())
        {
                forn(j,a[i].size())
                        suma[j]+=1.0*a[i][j];
        }
        double n=0;
        forn(i,a.size())
        {
                double ni=0,infoi=0;
                forn(j,a[i].size())
				{
					if(suma[j]==0)
						continue;
		            ni+=1.0*a[i][j]/suma[j];
				}

                forn(j,a[i].size())	
				{
					if(suma[j]==0||ni==0)
						continue;
		            infoi+=xlogx(1.0*a[i][j]/(suma[j]*ni));
				}
                ans+=ni*infoi;
                n+=ni;
        }

        return n==0?ans:ans/n;

}


double DecisionTree::contInfoGain(int f,double thres,vector<int> examples)
{
	vector<int> less(classes.size(),0);
	vector<int> more(classes.size(),0);
	vector<int> countclss(classes.size(),0);


	forn(i,examples.size())
	{
		int ex=examples[i];
		double val=stod(data[f][ex]);
		int clss=classes[data.back()[ex]];
		if(val<=thres)
			less[clss]++;
		else
			more[clss]++;

		countclss[clss]++;
		
	}

	vector<vector<int> > a;
	vector<vector<int> > olda;
	a.pb(less);
	a.pb(more);
	olda.pb(countclss);
	return C5CalcEntropy(olda)-C5CalcEntropy(a);

}


int globalcount=0;

void DecisionTree::Build(vector<int> examples,DTNode* root,vector<int> featUsed,string majorityclss)
{
	//vector<string> classes=meta.back();
	if(examples.size()==0)
	{
		root->setLeaf(true);
		root->setClass(majorityclss);
		cout<<globalcount++<<endl;
	}




	map<string,int> countclss;
	for(auto clss:classes)
		countclss[clss.first]=0;
	for(int i:examples)
		countclss[data.back()[i]]++;
	int maxcount=0,numclss=0;
	string majority="";

	for(auto it:countclss)
	{
		if(it.second>0)
			numclss++;
		if(maxcount>it.second)
			maxcount=it.second,majority=it.first;
		cout<<"Class name "<<it.first<<" class count"<<it.second<<endl;
	}

	majorityclss=majority.compare("")==0?majorityclss:majority;
	if(numclss==1)
	{
		root->setLeaf(true);
		root->setClass(majorityclss);
		cout<<globalcount++<<endl;

	}



	double ig=0,ansthres=-1;
	int bestF=-1;


	forn(f,data.size()-1)
	{
		if(types[f]==0)
		{
			double temp=0;	
			vector<double> thresholds;
			double minval=DBL_MAX,maxval=-DBL_MAX;

			for(int i:examples)
			{
					double val=stod(data[f][i]);
					if(minval>val)
						minval=val;
					if(maxval<val)
						maxval=val;
			}
			for(double thres:metaC[f])
				if(thres>=minval && thres<maxval)
				thresholds.pb(thres);
			double tempthres=-1;

			forn(attr,thresholds.size())
			{
				double temp2=contInfoGain(f,thresholds[attr],examples);
				if(temp2>temp)
				{
					temp=temp2;
					tempthres=thresholds[attr];
				}
			}

			if(temp>ig)
				bestF=f,ig=temp,ansthres=tempthres;



		}


		else
			continue;

	}

	cout<<"Info gain "<<ig<<endl;
	if(ig==0)
	{
		root->setLeaf(true);
		root->setClass(majorityclss);
		cout<<globalcount++<<endl;
		return;
	}



	root->setLeaf(false);
	root->setFeature(bestF);
	root->setType(types[bestF]);


	if(types[bestF]==0)
	{
		root->setThres(ansthres);
		vector<int> ex1;
		vector<int> ex2;

		for(int i:examples)
		{
			if(stod(data[bestF][i])<=ansthres)
				ex1.pb(i);
			else
				ex2.pb(i);
		}

		vector<DTNode*> children;
		DTNode* child1=new DTNode();
		DTNode* child2=new DTNode();
		children.pb(child1);
		children.pb(child2);
		root->setChildren(children);

		Build(ex1,child1,featUsed,majorityclss);
		Build(ex2,child2,featUsed,majorityclss);

	}

	else
		return;










	return;
}
void DecisionTree::Test(vector<vector<string> > testData,DTNode* root)
{
	DTNode* temp=root;
	int correct=0;

    for(int i=0;i<testData[0].size();i++)
    {

		while(temp!=NULL)
		{
			//cout<<temp->isLeaf()<<"What !!"<<endl;
			if(temp->isLeaf()==true)
			{
				if(temp->getClass().compare("1")!=0)
				cout<<"PREDICTED "<<temp->getClass()<<" ACTUAL "<<testData.back()[i]<<endl;
				if(temp->getClass().compare(testData.back()[i])==0)
				{
					correct++;
					
				}
				break;
			}

			if(temp->getType()==0)
			{
				int f=temp->getFeature();
				double thres=temp->getThres();
				vector<DTNode*> children=temp->getChildren();
				if(stod(testData[f][i])<=thres)
					temp=children[0];
				else
					temp=children[1];
			}
		}
	}
	cout<<"Total number "<<testData[0].size()<<" and number of correct "<<correct<<endl;
	return;
}


////////// Load data declarations /////////

vector<vector<string> > loadData::load(string fileName)
{
	ifstream fin(fileName,ios::in);
	vector<vector<string> > data;
	int count=0;
	string s;
	while(getline(fin,s)){

		vector<string> att;
		stringstream str(s);
		string temp;
		for (istringstream iss(s);iss >> temp;)
				att.push_back(temp);
		data.push_back(att);
	}

	if(data.size()==0)
		return data;


	vector<vector<string> > newData(data[0].size());


	for(int i=0;i<data.size();i++)
	{
		for(int j=0;j<data[i].size();j++)
			newData[j].push_back(data[i][j]);
	}


	return newData;
}


vector<vector<double> > loadData::loadMeta(vector<vector<string> > data)
{
	vector<vector<double> > meta;

	forn(i,data.size())
	{
		vector<double> temp;
		for(string s:data[i])
			temp.pb(stod(s));
		sort(temp.begin(),temp.end());
		vector<double> temp2;

		forn1(i,temp.size())
			if(temp[i]!=temp[i-1])
			temp2.pb((temp[i]+temp[i-1])/2);

		meta.pb(temp2);
	}

	return meta;

}



